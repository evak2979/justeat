﻿using System.Collections.Generic;

namespace JustEat.Models
{
    public class RestaurantsInformation
    {
        public List<Restaurant> Restaurants { get; set; }
    }
}
