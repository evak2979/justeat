﻿using System.Collections.Generic;

namespace JustEat.Models
{
    public class Restaurant
    {
        public decimal RatingAverage { get; set; }
        public string Name { get; set; }
        public List<CuisineType> CuisineTypes { get; set; }
    }
}