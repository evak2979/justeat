﻿using static System.Console;
using System.Linq;
using JustEat.Infrastructure.BusinessModels;
using JustEat.Infrastructure.Concretes;
using JustEat.Models;

namespace JustEat.BasicUI
{
    class Program
    {
        static void Main(string[] args)
        {
            var justEatGetRequestTemplate = new JustEatGETRequestTemplate(new HttpClientFactory(), new JustEatSerializer(),
                new JustEatApiHttpRequestMessageBuilderDirector(new HttpRequestMessageBuilder()));
            var basicRestaurantInformationModelMapper = new BasicRestaurantInformationModelMapper();


            WriteLine("Please enter an outcode:");
            var outCode = ReadLine();

            var restaurantsInformation =
                justEatGetRequestTemplate.Get<RestaurantsInformation>(
                    $"https://public.je-apis.com/restaurants?q={outCode}").Result;

            var basicRestaurantInformationList = basicRestaurantInformationModelMapper.Map(restaurantsInformation);

            if(!basicRestaurantInformationList.Any())
            {
                WriteLine($"No restaurants found in outcode: {outCode}");
            }
            else
            {
                foreach (var basicRestaurantInformation in basicRestaurantInformationList)
                {
                    OutputRestaurantInformation(basicRestaurantInformation);
                    foreach (var basicCuisineInformation in basicRestaurantInformation.BasicCuisineInformations)
                    {
                        OutputCuisineInformation(basicCuisineInformation);
                    }

                    WriteLine();
                }
            }

            WriteLine("Thank you! Please press any key to exit!");
            ReadLine();
        }

        private static void OutputCuisineInformation(BasicCuisineInformation basicCuisineInformation)
        {
            WriteLine($"\t\t{basicCuisineInformation.TypeOfFood}");
        }

        private static void OutputRestaurantInformation(BasicRestaurantInformation basicRestaurantInformation)
        {
            WriteLine($"Name: {basicRestaurantInformation.Name}");
            WriteLine($"Rating: {basicRestaurantInformation.Rating}");
            WriteLine("Types of food:");
        }
    }
}
