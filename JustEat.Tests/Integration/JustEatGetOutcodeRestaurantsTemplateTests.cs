﻿using System.Linq;
using System.Threading.Tasks;
using JustEat.Infrastructure.Concretes;
using JustEat.Models;
using Shouldly;
using Xunit;

namespace JustEat.Tests.Integration
{
    public class JustEatGetOutcodeRestaurantsTemplateTests
    {
        private JustEatGETRequestTemplate _subject;

        public JustEatGetOutcodeRestaurantsTemplateTests()
        {
            _subject = new JustEatGETRequestTemplate(new HttpClientFactory(), new JustEatSerializer(), 
                new JustEatApiHttpRequestMessageBuilderDirector(new HttpRequestMessageBuilder()));
        }

        [Fact]
        public async Task ShouldReturnAListOfRestaurantsFromValidEndpoint()
        {
            // setup + act
            var result = await _subject.Get<RestaurantsInformation>("https://public.je-apis.com/restaurants?q=se19");

            // assert
            result.Restaurants.Any().ShouldBeTrue();
        }
    }
}
