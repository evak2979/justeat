﻿using System.Collections.Generic;
using System.Net.Http;
using JustEat.Infrastructure.Interfaces;

namespace JustEat.Tests.Stubs
{
    public class HttpRequestMessageBuilderStub : IHttpRequestMessageBuilder
    {
        public Dictionary<string, string> Headers { get; set; }

        public HttpMethod HttpMethod { get; set; }

        public string Url { get; set; }

        public HttpRequestMessage HttpRequestMessage { get; set; }

        public HttpRequestMessageBuilderStub()
        {
            Headers = new Dictionary<string, string>();
            HttpRequestMessage = new HttpRequestMessage();
        }
        
        public IHttpRequestMessageBuilder WithMethod(HttpMethod httpMethod)
        {
            HttpMethod = httpMethod;

            return this;
        }

        public IHttpRequestMessageBuilder WithUrl(string url)
        {
            Url = url;

            return this;
        }

        public IHttpRequestMessageBuilder AddHeader(string key, string value)
        {
            Headers.Add(key, value);

            return this;
        }

        public HttpRequestMessage Build()
        {
            return HttpRequestMessage;
        }
    }
}