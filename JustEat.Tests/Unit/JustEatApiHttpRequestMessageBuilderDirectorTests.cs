﻿using System.Net.Http;
using JustEat.Infrastructure.Concretes;
using JustEat.Tests.Stubs;
using Shouldly;
using Xunit;

namespace JustEat.Tests.Unit
{
    public class JustEatApiHttpRequestMessageBuilderDirectorTests
    {
        private HttpRequestMessageBuilderStub _httpRequestMessageBuilderStub;
        private JustEatApiHttpRequestMessageBuilderDirector _subject;

        private string _requestUrl = "http://www.somewhereintime.com/seventh/son";
        private HttpMethod _httpMethod = HttpMethod.Get;

        public JustEatApiHttpRequestMessageBuilderDirectorTests()
        {
            _httpRequestMessageBuilderStub = new HttpRequestMessageBuilderStub();

            _subject = new JustEatApiHttpRequestMessageBuilderDirector(_httpRequestMessageBuilderStub);
        }

        [Fact]
        public void ShouldCallWithMethodOnTheMessageBuilderWithTheHttpMethodParameter()
        {
            // setup + act
            _subject.Build(_requestUrl, _httpMethod);

            // assert
            _httpRequestMessageBuilderStub.HttpMethod.ShouldBe(_httpMethod);
        }

        [Fact]
        public void ShouldCallAddHeaderOnMessageBuilderForAcceptTenantHeader()
        {
            // setup + act
            _subject.Build(_requestUrl, _httpMethod);

            // assert
            _httpRequestMessageBuilderStub.Headers["Accept-Tenant"].ShouldBe("uk");
        }

        [Fact]
        public void ShouldCallAddHeaderOnMessageBuilderForAcceptLanguageHeader()
        {
            // setup + act
            _subject.Build(_requestUrl, _httpMethod);

            // assert
            _httpRequestMessageBuilderStub.Headers["Accept-Language"].ShouldBe("en-GB");
        }

        [Fact]
        public void ShouldCallAddHeaderOnMessageBuilderForAuthorizationHeader()
        {
            // setup + act
            _subject.Build(_requestUrl, _httpMethod);

            // assert
            _httpRequestMessageBuilderStub.Headers["Authorization"].ShouldBe("Basic VGVjaFRlc3Q6bkQ2NGxXVnZreDVw");
        }

        [Fact]
        public void ShouldCallAddHeaderOnMessageBuilderForHostHeader()
        {
            // setup + act
            _subject.Build(_requestUrl, _httpMethod);

            // assert
            _httpRequestMessageBuilderStub.Headers["Host"].ShouldBe("public.je-apis.com");
        }

        [Fact]
        public void ShouldCallWithUrlOnMessageBuilderWithTheUrlParameter()
        {
            // setup + act
            _subject.Build(_requestUrl, _httpMethod);

            // assert
            _httpRequestMessageBuilderStub.Url.ShouldBe(_requestUrl);
        }

        [Fact]
        public void ShouldCallBuildOnMessageBuilder()
        {
            // setup + act
            var result = _subject.Build(_requestUrl, _httpMethod);

            // assert
            result.ShouldBe(_httpRequestMessageBuilderStub.HttpRequestMessage);
        }
    }
}
