﻿using System.Linq;
using System.Net.Http;
using JustEat.Infrastructure.Concretes;
using Shouldly;
using Xunit;

namespace JustEat.Tests.Unit
{
    public class HttpRequestMessageBuilderTests
    {
        private HttpRequestMessageBuilder _subject;

        public HttpRequestMessageBuilderTests()
        {
            _subject = new HttpRequestMessageBuilder();
        }

        [Fact]
        public void BuildShouldReturnANewHttpRequestMessage()
        {
            // setup + act
            var result = _subject
                .Build();

            // assert
            result.ShouldNotBeNull();
            result.ShouldBeOfType<HttpRequestMessage>();
        }

        [Fact]
        public void AddHeaderShouldAddAHeaderToTheHttpRequestMessage()
        {
            // setup + act
            var result = _subject
                .AddHeader("Some", "Header")
                .Build();

            // assert
            result.Headers.First().Key.ShouldBe("Some");
            result.Headers.First().Value.First().ShouldBe("Header");
        }

        [Fact]
        public void WithMethodShouldSetTheHttpRequestMessageMethod()
        {
            // setup + act
            var result = _subject
                .WithMethod(HttpMethod.Get)
                .Build();

            // assert
            result.Method.ShouldBe(HttpMethod.Get);
        }

        [Fact]
        public void WithUrlShouldSetTheHttpRequestMessageUrl()
        {
            // setup
            var requestUrl = "http://www.somewhereintime.com/seventh/son";

            // // act
            var result = _subject
                .WithUrl(requestUrl)
                .Build();

            // assert
            result.RequestUri.AbsoluteUri.ShouldBe(requestUrl);
        }
    }
}
