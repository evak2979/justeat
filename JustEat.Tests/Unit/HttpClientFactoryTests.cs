﻿using JustEat.Infrastructure.Concretes;
using Shouldly;
using Xunit;

namespace JustEat.Tests.Unit
{
    public class HttpClientFactoryTests
    {
        private readonly HttpClientFactory _subject;

        public HttpClientFactoryTests()
        {
            _subject = new HttpClientFactory();
        }

        [Fact]
        public void ShouldRetrieveASingletonInstanceOfTheHttpClient()
        {
            // setup 
            var firstHttpClient = _subject.Create();

            // act
            var secondHttpClient = _subject.Create();

            // assert
            firstHttpClient.ShouldBe(secondHttpClient);
        }
    }
}
