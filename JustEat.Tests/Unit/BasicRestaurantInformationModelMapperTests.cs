﻿using System.Collections.Generic;
using System.Linq;
using JustEat.Infrastructure.Concretes;
using JustEat.Models;
using Shouldly;
using Xunit;

namespace JustEat.Tests.Unit
{
    public class BasicRestaurantInformationModelMapperTests
    {
        private RestaurantsInformation _restaurantInformation;
        private BasicRestaurantInformationModelMapper _subject;

        public BasicRestaurantInformationModelMapperTests()
        {
            _restaurantInformation = SetupRestaurantInfo();

            _subject = new BasicRestaurantInformationModelMapper();
        }

        [Fact]
        public void ShouldReturnEmptyListIfRestaurantInformationIsNull()
        {
            // setup + act
            var result = _subject.Map(null);

            // assert
            result.ShouldNotBeNull();
            result.Any().ShouldBeFalse();
        }

        [Fact]
        public void ShouldReturnEmptyListIfRestaurantInformationRestaurantsIsNull()
        {
            // setup + act
            var result = _subject.Map(new RestaurantsInformation
            {
                Restaurants = null
            });

            // assert
            result.ShouldNotBeNull();
            result.Any().ShouldBeFalse();
        }

        [Fact]
        public void ShouldProperlyMapARestaurantInformationToABasicRestaurantInformationList()
        {
            // setup + act
            var result = _subject.Map(_restaurantInformation);

            // assert
            result[0].Name.ShouldBe(_restaurantInformation.Restaurants[0].Name);
            result[0].Rating.ShouldBe(_restaurantInformation.Restaurants[0].RatingAverage);
            result[0].BasicCuisineInformations[0].TypeOfFood.ShouldBe(_restaurantInformation.Restaurants[0].CuisineTypes[0].Name);
            result[1].Name.ShouldBe(_restaurantInformation.Restaurants[1].Name);
            result[1].Rating.ShouldBe(_restaurantInformation.Restaurants[1].RatingAverage);
            result[1].BasicCuisineInformations[0].TypeOfFood.ShouldBe(_restaurantInformation.Restaurants[1].CuisineTypes[0].Name);
        }

        private RestaurantsInformation SetupRestaurantInfo()
        {
            return new RestaurantsInformation
            {
                Restaurants = new List<Restaurant>
                {
                    new Restaurant
                    {
                        Name = "FirstName",
                        RatingAverage = 12.345m,
                        CuisineTypes = new List<CuisineType>
                        {
                            new CuisineType
                            {
                                Name = "FirstCuisine"
                            }
                        }
                    },
                    new Restaurant
                    {
                        Name = "FirstName",
                        RatingAverage = 12.345m,
                        CuisineTypes = new List<CuisineType>
                        {
                            new CuisineType
                            {
                                Name = "FirstCuisine"
                            }
                        }
                    },
                }
            };
        }
    }
}
