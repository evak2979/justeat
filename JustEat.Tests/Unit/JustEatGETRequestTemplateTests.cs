﻿using System.Net.Http;
using System.Threading.Tasks;
using JustEat.Infrastructure.Concretes;
using JustEat.Infrastructure.Interfaces;
using Moq;
using Shouldly;
using Xunit;

namespace JustEat.Tests.Unit
{
    public class JustEatGETRequestTemplateTests
    {
        private Mock<IHttpClientFactory> _mockHttpClientFactory;
        private Mock<ISerializer> _mockSerializer;
        private Mock<IHttpRequestMessageBuilderDirector> _mockHttpRequestMessageDirector;
        private Mock<IHttpClientWrapper> _mockHttpClientWrapper;

        private string _requestUrl = "www.painkiller.com";
        private HttpResponseMessage _httpResponseMessage;

        private JustEatGETRequestTemplate _subject;

        public JustEatGETRequestTemplateTests()
        {
            _httpResponseMessage = new HttpResponseMessage();
            _httpResponseMessage.Content = new StringContent("");

            _mockHttpClientFactory = new Mock<IHttpClientFactory>();
            _mockHttpRequestMessageDirector = new Mock<IHttpRequestMessageBuilderDirector>();
            _mockSerializer = new Mock<ISerializer>();
            _mockHttpClientWrapper = new Mock<IHttpClientWrapper>();
            _mockHttpClientFactory.Setup(x => x.Create()).Returns(_mockHttpClientWrapper.Object);
            _mockHttpClientWrapper.Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>()))
                .ReturnsAsync(_httpResponseMessage);

            _subject = new JustEatGETRequestTemplate(_mockHttpClientFactory.Object, _mockSerializer.Object, 
                _mockHttpRequestMessageDirector.Object);
        }

        [Fact]
        public async Task ShouldCallHttpClientFactoryToCreateAnHttpClient()
        {
            // setup + act
            await _subject.Get<object>(_requestUrl);

            // assert
            _mockHttpClientFactory.Verify(x => x.Create());
        }

        [Fact]
        public async Task ShouldCallHttpRequestMessageDirectorToCreateAnHttpRequestMessage()
        {
            // setup + act
            await _subject.Get<object>(_requestUrl);

            // assert
            _mockHttpRequestMessageDirector.Verify(x => x.Build(_requestUrl, HttpMethod.Get));
        }

        [Fact]
        public async Task ShouldPassTheCreatedRequestMessageToTheCreatedHttpClient()
        {
            // setup
            var requestMessage = new HttpRequestMessage();
            _mockHttpRequestMessageDirector.Setup(x => x.Build(_requestUrl, HttpMethod.Get)).Returns(requestMessage);

            // act
            await _subject.Get<object>(_requestUrl);

            // assert
            _mockHttpClientWrapper.Verify(x => x.SendAsync(requestMessage));
        }

        [Fact]
        public async Task ShouldCallSerializerToSerializeResponseMessageContent()
        {
            // setup + act
            await _subject.Get<object>(_requestUrl);

            // assert
            _mockSerializer.Verify(x => x.Serialize<object>(_httpResponseMessage.Content.ReadAsStringAsync().Result));
        }

        [Fact]
        public async Task ShouldReturnSerializationResult()
        {
            // setup
            var returnObject = new object();
            _mockSerializer.Setup(x => x.Serialize<object>(_httpResponseMessage.Content.ReadAsStringAsync().Result))
                .Returns(returnObject);

            // act
            var result = await _subject.Get<object>(_requestUrl);

            // assert
            result.ShouldBe(returnObject);
        }
    }
}
