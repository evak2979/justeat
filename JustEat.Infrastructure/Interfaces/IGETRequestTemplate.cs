﻿using System.Threading.Tasks;

namespace JustEat.Infrastructure.Interfaces
{
    public interface IGETRequestTemplate
    {
        Task<T> Get<T>(string url) where T : class;
    }
}