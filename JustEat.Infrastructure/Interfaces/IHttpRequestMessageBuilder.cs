﻿using System.Net.Http;

namespace JustEat.Infrastructure.Interfaces
{
    public interface IHttpRequestMessageBuilder
    {
        IHttpRequestMessageBuilder WithMethod(HttpMethod httpMethod);

        IHttpRequestMessageBuilder WithUrl(string url);

        IHttpRequestMessageBuilder AddHeader(string key, string value);

        System.Net.Http.HttpRequestMessage Build();
    }
}
