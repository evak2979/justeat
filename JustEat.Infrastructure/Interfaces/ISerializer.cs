﻿namespace JustEat.Infrastructure.Interfaces
{
    public interface ISerializer
    {
        T Serialize<T>(string value);
    }
}