﻿using System.Net.Http;

namespace JustEat.Infrastructure.Interfaces
{
    public interface IHttpRequestMessageBuilderDirector
    {
        HttpRequestMessage Build(string url, HttpMethod httpMethod);
    }
}