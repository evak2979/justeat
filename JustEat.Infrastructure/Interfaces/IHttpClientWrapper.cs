﻿using System.Net.Http;
using System.Threading.Tasks;

namespace JustEat.Infrastructure.Interfaces
{
    public interface IHttpClientWrapper
    {
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage httpRequestMessage);
    }
}