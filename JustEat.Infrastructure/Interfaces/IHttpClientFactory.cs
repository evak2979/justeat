﻿namespace JustEat.Infrastructure.Interfaces
{
    public interface IHttpClientFactory
    {
        IHttpClientWrapper Create();
    }
}