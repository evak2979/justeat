﻿using System.Collections.Generic;
using JustEat.Models;

namespace JustEat.Infrastructure.Interfaces
{
    interface IRestaurantsInformationModelMapper<T>
    {
        List<T> Map(RestaurantsInformation restaurantInfo);
    }
}
