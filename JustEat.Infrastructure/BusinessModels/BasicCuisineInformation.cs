﻿namespace JustEat.Infrastructure.BusinessModels
{
    public class BasicCuisineInformation
    {
        public string TypeOfFood { get; set; }
    }
}