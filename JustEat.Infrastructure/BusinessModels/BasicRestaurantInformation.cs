﻿using System.Collections.Generic;

namespace JustEat.Infrastructure.BusinessModels
{
    public class BasicRestaurantInformation
    {
        public string Name { get; set; }

        public decimal Rating { get; set; }

        public List<BasicCuisineInformation> BasicCuisineInformations { get; set; }
    }
}
