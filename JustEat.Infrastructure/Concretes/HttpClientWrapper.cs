﻿using System.Net.Http;
using System.Threading.Tasks;
using JustEat.Infrastructure.Interfaces;

namespace JustEat.Infrastructure.Concretes
{
    public class HttpClientWrapper : IHttpClientWrapper
    {
        private readonly HttpClient _httpClient;

        public HttpClientWrapper(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public Task<HttpResponseMessage> SendAsync(HttpRequestMessage httpRequestMessage)
        {
            return _httpClient.SendAsync(httpRequestMessage);
        }
    }
}