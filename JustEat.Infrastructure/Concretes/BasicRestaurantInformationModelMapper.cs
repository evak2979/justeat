﻿using System;
using System.Collections.Generic;
using System.Linq;
using JustEat.Infrastructure.BusinessModels;
using JustEat.Models;
using JustEat.Infrastructure.Interfaces;

namespace JustEat.Infrastructure.Concretes
{
    public class BasicRestaurantInformationModelMapper : IRestaurantsInformationModelMapper<BasicRestaurantInformation>
    {
        public List<BasicRestaurantInformation> Map(RestaurantsInformation restaurantInfo)
        {
            var basicRestaurantInformationList = new List<BasicRestaurantInformation>();

            if (restaurantInfo?.Restaurants == null)
                return basicRestaurantInformationList;

            foreach (var restaurant in restaurantInfo.Restaurants)
            {
                var basicRstaurantInformation = new BasicRestaurantInformation
                {
                    Name = restaurant.Name,
                    Rating = restaurant.RatingAverage
                };

                basicRstaurantInformation.BasicCuisineInformations =
                    restaurant.CuisineTypes.Select(GetBasicCuisineInformations()).ToList();

                basicRestaurantInformationList.Add(basicRstaurantInformation);
            }

            return basicRestaurantInformationList;
        }

        private static Func<CuisineType, BasicCuisineInformation> GetBasicCuisineInformations()
        {
            return cuisineType => new BasicCuisineInformation
            {
                TypeOfFood = cuisineType.Name
            };
        }
    }
}