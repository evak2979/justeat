﻿using System;
using System.Net.Http;
using JustEat.Infrastructure.Interfaces;

namespace JustEat.Infrastructure.Concretes
{
    public class HttpRequestMessageBuilder : IHttpRequestMessageBuilder
    {
        private HttpRequestMessage _httpRequestMessage;

        public HttpRequestMessageBuilder()
        {
            _httpRequestMessage = new HttpRequestMessage();
        }

        public IHttpRequestMessageBuilder WithMethod(HttpMethod httpMethod)
        {
            _httpRequestMessage.Method = httpMethod;

            return this;
        }

        public IHttpRequestMessageBuilder WithUrl(string url)
        {
            _httpRequestMessage.RequestUri = new Uri(url);

            return this;
        }

        public IHttpRequestMessageBuilder AddHeader(string key, string value)
        {
            _httpRequestMessage.Headers.Add(key, value);
            
            return this;
        }

        public HttpRequestMessage Build()
        {
            return _httpRequestMessage;
        }
    }
}