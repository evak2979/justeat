﻿using System.Net.Http;
using System.Threading.Tasks;
using JustEat.Infrastructure.Interfaces;

namespace JustEat.Infrastructure.Concretes
{
    public class JustEatGETRequestTemplate : IGETRequestTemplate
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ISerializer _serializer;
        private readonly IHttpRequestMessageBuilderDirector _justEatHttpRequestMessageBuilderDirector;

        public JustEatGETRequestTemplate(IHttpClientFactory httpClientFactory, ISerializer serializer, IHttpRequestMessageBuilderDirector justEatHttpRequestMessageBuilderDirector)
        {
            _httpClientFactory = httpClientFactory;
            _serializer = serializer;
            _justEatHttpRequestMessageBuilderDirector = justEatHttpRequestMessageBuilderDirector;
        }

        public async Task<T> Get<T>(string url) where T : class
        {
            var httpClientWrapper = _httpClientFactory.Create();
            var httpRequestMessage = _justEatHttpRequestMessageBuilderDirector.Build(url, HttpMethod.Get);

            var httpResponseMessage = await httpClientWrapper.SendAsync(httpRequestMessage);
            var responseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return _serializer.Serialize<T>(responseContent);
        }
    }
}