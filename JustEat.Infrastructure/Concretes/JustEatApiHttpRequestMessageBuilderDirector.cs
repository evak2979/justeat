﻿using System.Net.Http;
using JustEat.Infrastructure.Interfaces;

namespace JustEat.Infrastructure.Concretes
{
    public class JustEatApiHttpRequestMessageBuilderDirector : IHttpRequestMessageBuilderDirector
    {
        private readonly IHttpRequestMessageBuilder _httpRequestMessageBuilder;

        public JustEatApiHttpRequestMessageBuilderDirector(IHttpRequestMessageBuilder httpRequestMessageBuilder)
        {
            _httpRequestMessageBuilder = httpRequestMessageBuilder;
        }

        public HttpRequestMessage Build(string url, HttpMethod httpMethod)
        {
            _httpRequestMessageBuilder
                .AddHeader("Accept-Tenant", "uk")
                .AddHeader("Accept-Language", "en-GB")
                .AddHeader("Authorization", "Basic VGVjaFRlc3Q6bkQ2NGxXVnZreDVw")
                .AddHeader("Host", "public.je-apis.com")
                .WithMethod(httpMethod)
                .WithUrl(url);

            return _httpRequestMessageBuilder.Build();
        }
    }
}