﻿using System.Net.Http;
using JustEat.Infrastructure.Interfaces;

namespace JustEat.Infrastructure.Concretes
{
    public class HttpClientFactory : IHttpClientFactory
    {
        private static HttpClientWrapper _httpClient;

        public IHttpClientWrapper Create()
        {
            if (_httpClient == null)
            {
                InitializeHttpClient();
            }

            return _httpClient;
        }

        private static void InitializeHttpClient()
        {
            _httpClient = new HttpClientWrapper(new HttpClient());
        }
    }
}