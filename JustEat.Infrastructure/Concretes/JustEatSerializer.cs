﻿using JustEat.Infrastructure.Interfaces;
using Newtonsoft.Json;

namespace JustEat.Infrastructure.Concretes
{
    public class JustEatSerializer : ISerializer
    {
        public T Serialize<T>(string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }
    }
}